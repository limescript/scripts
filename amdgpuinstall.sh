#! /bin/bash

############################################################
###		      Arch Linux AMDGPU telepítő script	         ###
###		            Multilib szükséges                   ###
############################################################

sudo pacman -S --noconfirm mesa lib32-mesa
sudo pacman -S --noconfirm xf86-video-amdgpu
sudo pacman -S --noconfirm vulkan-radeon lib32-vulkan-radeon

echo "################################################################"
echo "#########       AMDGPU sikeresen telepítve      ################"
echo "################################################################"